export class DefaultResponseDto<T> {
  error: boolean;
  message: string;
  data: T;
}
