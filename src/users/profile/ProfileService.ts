import { autoInjectable, inject, registry } from "tsyringe";
import { UserEntity, UserPostRegisterDto, UserPostUpdateDto, UserService } from "@User";

@autoInjectable()
export class ProfileService {
  constructor( protected userService: UserService) { }

  getUserToken(user:UserEntity){
    return this.userService.getUserToken(user);
  }

  deleteUserToken(hash:string){
    return this.userService.deleteLoginToken(hash);
  }

  async testLoginToken(hash: string){
    return this.userService.testLoginToken(hash);
  }

  register(userData: UserPostRegisterDto) {
    return this.userService.registerUser(userData);
  }

  getOne(userId: number) {
    return this.userService.getOne(userId);
  }

  getList() {
    return this.userService.getList();
  }

  triggerBlockUser(userId: number) {
    return this.userService.triggerBlockUser(userId);
  }

  deleteUser(userId: number) {
    return this.userService.deleteUser(userId);
  }

  updateUser(userId: number, userData: UserPostUpdateDto) {
    return this.userService.updateUser(userId, userData);
  }

  setUserAdminRights(userId: number, isAdmin: boolean) {
    return this.userService.setUserAdminRights(userId, isAdmin);
  }
}
