import { UserPostRegisterDto, IUser, UserPostUpdateDto, UserEntity, JwtDto } from "@User";
import { inject, autoInjectable, delay } from "tsyringe";
import { ProfileService } from "./ProfileService";
import { DefaultResponseDto } from "../Dtos/DefaultResponseDto";
import { Request as Req, Response as Res } from "express";
import { verify } from "jsonwebtoken";
import { Config } from "@Config";

@autoInjectable()
export class ProfileController {
  constructor(@inject(delay(() => ProfileService)) protected service: ProfileService, protected config: Config) { }

  async testUserJwt(req: Req & { user: IUser }, res: Res) {
    const defaultResponseDto: DefaultResponseDto<boolean> = new DefaultResponseDto<boolean>();
    try {
      if (!req.user.isService) {
        throw new Error("Access denied");
      }
      const tokenHash = this.getUserToken(req.header("X-User-Token"));
      if (tokenHash === null && tokenHash.length === 0) {
        throw new Error("Token not found");
      }
      const jwt = this.decodeJwt(tokenHash);
      defaultResponseDto.error = true;
      defaultResponseDto.message = "";
      defaultResponseDto.data = await this.service.testLoginToken(jwt.hash);
    } catch (e) {
      res.statusCode = 400;
      res.statusMessage = e.message;
      defaultResponseDto.error = true;
      defaultResponseDto.message = e.message;
      defaultResponseDto.data = null;
    } finally {
      res.json(defaultResponseDto);
    }
  }

  async loginUserGetToken(req: Req & { user: IUser }, res: Res) {
    const defaultResponseDto: DefaultResponseDto<string> = new DefaultResponseDto<string>();
    try {
      defaultResponseDto.error = true;
      defaultResponseDto.message = "";
      defaultResponseDto.data = (await this.service.getUserToken(await this.service.getOne(req.user.id)));
    } catch (e) {
      res.statusCode = 400;
      res.statusMessage = e.message;
      defaultResponseDto.error = true;
      defaultResponseDto.message = e.message;
      defaultResponseDto.data = null;
    } finally {
      res.json(defaultResponseDto);
    }
  }

  async deleteUserToken(req: Req & { user: IUser }, res: Res) {
    const defaultResponseDto: DefaultResponseDto<boolean> = new DefaultResponseDto<boolean>();
    try {
      const tokenHash = this.getUserToken(req.header("Authorization"));
      if (tokenHash === null && tokenHash.length === 0) {
        throw new Error("Token not found");
      }
      const jwt = this.decodeJwt(tokenHash);
      defaultResponseDto.error = true;
      defaultResponseDto.message = "";
      defaultResponseDto.data = (await this.service.deleteUserToken(jwt.hash)).affected > 0;
    } catch (e) {
      res.statusCode = 400;
      res.statusMessage = e.message;
      defaultResponseDto.error = true;
      defaultResponseDto.message = e.message;
      defaultResponseDto.data = null;
    } finally {
      res.json(defaultResponseDto);
    }
  }

  protected decodeJwt(jwt: string): JwtDto {
    return verify(jwt, this.config.get('JWT_SECRET_KEY')) as JwtDto;
  }

  protected getUserToken(auth: string) {
    if (auth.length > 0 && auth.indexOf(" ") > 0) {
      return auth[1];
    } return null;
  }

  async registerUser(req: Req, res: Res) {
    const defaultResponseDto: DefaultResponseDto<number> = new DefaultResponseDto<number>();
    try {
      res.statusCode = 200;
      res.statusMessage = "Ok";
      defaultResponseDto.error = false;
      defaultResponseDto.message = "Ok";
      defaultResponseDto.data = await this.service.register(this.getRegisterDto(req.body));
    } catch (e) {
      res.statusCode = 400;
      res.statusMessage = e.message;
      defaultResponseDto.error = true;
      defaultResponseDto.message = e.message;
      defaultResponseDto.data = null;
    } finally {
      res.json(defaultResponseDto);
    }
  }
  async registerUserByAdmin(req: Req & { user: IUser }, res: Res) {
    const defaultResponseDto: DefaultResponseDto<number> = new DefaultResponseDto<number>();
    try {
      if ('user' in req && req.user && 'id' in req.user && Number(req.user.id) > 0 && req.user.isActive && req.user.isAdmin) {
        res.statusCode = 200;
        res.statusMessage = "Ok";
        defaultResponseDto.error = false;
        defaultResponseDto.message = "Ok";
        defaultResponseDto.data = await this.service.register(this.getRegisterDto(req.body));
      } else {
        throw new Error("Access denied")
      }
    } catch (e) {
      res.statusCode = 400;
      res.statusMessage = e.message;
      defaultResponseDto.error = true;
      defaultResponseDto.message = e.message;
      defaultResponseDto.data = null;
    } finally {
      res.json(defaultResponseDto);
    }
  }
  protected getRegisterDto(body) {
    const userData: UserPostRegisterDto = new UserPostRegisterDto();
    if ('username' in body && body.username.length > 0) {
      userData.username = body.username;
    } else {
      throw new Error(`Field 'username' not found`);
    }
    if ('password' in body && body.username.length > 0) {
      userData.password = body.password;
    } else {
      throw new Error(`Field 'password' not found`);
    }
    return userData;
  }

  async getUser(req: Req & { user: IUser }, res: Res) {
    const result = new DefaultResponseDto<UserEntity>();
    try {
      if ('user' in req && req.user && 'id' in req.user && Number(req.user.id) > 0) {
        if (!req.user.isActive) {
          throw new Error("User is not active");
        }
        result.error = false;
        result.data = await this.service.getOne(req.user.id);
        result.message = "";
      } else {
        throw new Error("Field 'user.id' not found")
      }
    } catch (e) {
      res.statusCode = 401;
      res.statusMessage = e.message;
      result.error = true;
      result.message = e.message;
      result.data = null;
    } finally {
      res.json(result);
    }
  }

  async getList(req: Req & { user: IUser }, res: Res) {
    const result = new DefaultResponseDto<UserEntity[]>();
    try {
      if ('user' in req && req.user && 'id' in req.user && Number(req.user.id) > 0 && req.user.isActive && req.user.isAdmin) {
        result.error = true;
        result.message = '';
        result.data = await this.service.getList();
      } else {
        throw new Error("Access denied")
      }
    } catch (e) {
      res.statusCode = 401;
      res.statusMessage = e.message;
      result.error = true;
      result.message = e.message;
      result.data = null;
    } finally {
      res.json(result);
    }
  }
  async updateUser(req: Req & { user: IUser }, res: Res) {
    const result = new DefaultResponseDto<UserEntity>();
    try {
      if ('user' in req && req.user && 'id' in req.user && Number(req.user.id) > 0) {
        if (!req.user.isActive) {
          throw new Error("User is not active");
        }
        result.error = false;
        result.data = await this.service.updateUser(req.user.id, this.getUpdateUserDto(req.body));
        result.message = "";
      } else {
        throw new Error("Field 'user.id' not found")
      }
    } catch (e) {
      res.statusCode = 401;
      res.statusMessage = e.message;
      result.error = true;
      result.message = e.message;
      result.data = null;
    } finally {
      res.json(result);
    }
  }
  async updateUserByAdmin(req: Req & { user: IUser, params: { id?: string } }, res: Res) {
    const result = new DefaultResponseDto<UserEntity>();
    try {
      if ('user' in req && req.user && 'id' in req.user && Number(req.user.id) > 0 && req.user.isAdmin) {
        if (!req.user.isActive) {
          throw new Error("User is not active");
        }
        result.error = false;
        result.data = await this.service.updateUser(Number(req.params.id), this.getUpdateUserDto(req.body));
        result.message = "";
      } else {
        throw new Error("Field 'user.id' not found")
      }
    } catch (e) {
      res.statusCode = 401;
      res.statusMessage = e.message;
      result.error = true;
      result.message = e.message;
      result.data = null;
    } finally {
      res.json(result);
    }
  }
  protected getUpdateUserDto(body) {
    const userData: UserPostUpdateDto = new UserPostUpdateDto();
    if ('username' in body && body.username.length > 0) {
      userData.username = body.username;
    }
    if ('password' in body && body.username.length > 0) {
      userData.password = body.password;
    }
    return userData;
  }
  async triggerBlockUser(req: Req & { user: IUser, params: { id?: string } }, res: Res) {
    const result = new DefaultResponseDto<boolean>();
    try {
      if ('user' in req && req.user && 'id' in req.user && Number(req.user.id) > 0 && req.user.isActive && req.user.isAdmin && 'id' in req.params) {
        result.error = true;
        result.message = '';
        result.data = await this.service.triggerBlockUser(Number(req.params.id));
      } else {
        throw new Error("Access denied")
      }
    } catch (e) {
      res.statusCode = 401;
      res.statusMessage = e.message;
      result.error = true;
      result.message = e.message;
      result.data = null;
    } finally {
      res.json(result);
    }
  }


  async setUserAdminRights(req: Req & { user: IUser, body: { isAdmin?: boolean }, params: { id?: string } }, res: Res) {
    const defaultResponseDto: DefaultResponseDto<UserEntity> = new DefaultResponseDto<UserEntity>();
    try {
      if (
        'user' in req && req.user &&
        'id' in req.user &&
        Number(req.user.id) > 0 &&
        req.user.isActive &&
        req.user.isAdmin &&
        'body' in req &&
        'isAdmin' in req.body &&
        'id' in req.params &&
        Number(req.params.id) > 0
      ) {
        res.statusCode = 200;
        res.statusMessage = "Ok";
        defaultResponseDto.error = false;
        defaultResponseDto.message = "Ok";
        defaultResponseDto.data = await this.service.setUserAdminRights(Number(req.params.id), Boolean(req.body.isAdmin));
      } else {
        throw new Error("Access denied")
      }
    } catch (e) {
      res.statusCode = 400;
      res.statusMessage = e.message;
      defaultResponseDto.error = true;
      defaultResponseDto.message = e.message;
      defaultResponseDto.data = null;
    } finally {
      res.json(defaultResponseDto);
    }
  }
  async deleteUser(req: Req & { user: IUser, params: { id?: string } }, res: Res) {
    const result = new DefaultResponseDto<boolean>();
    try {
      if ('user' in req && req.user && 'id' in req.user && Number(req.user.id) > 0 && req.user.isActive && req.user.isAdmin && 'id' in req.params) {
        result.error = true;
        result.message = '';
        result.data = (await this.service.deleteUser(Number(req.params.id))).affected > 0;
      } else {
        throw new Error("Access denied")
      }
    } catch (e) {
      res.statusCode = 401;
      res.statusMessage = e.message;
      result.error = true;
      result.message = e.message;
      result.data = null;
    } finally {
      res.json(result);
    }
  }
}
