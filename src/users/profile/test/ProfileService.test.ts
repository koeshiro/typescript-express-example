import "reflect-metadata";
import { FindConditions, SaveOptions, DeleteResult } from "typeorm";
import { Config } from "@Config";
import { UserEntity, UserRepository, UserPostRegisterDto, UserPostUpdateDto, UserService, TokenRepository } from "@User";
import { ProfileService } from "../ProfileService";
jest.mock("@User/repositories/User.repository");
jest.mock("@User/repositories/Token.repository");
jest.unmock("@User/UserService")
require('dotenv').config();
describe("profile service", () => {
  it("register user", async () => {
    const userRepository = new UserRepository();
    const tokenRepository = new TokenRepository();
    const spyCreate = jest.spyOn(userRepository, 'create').mockImplementation(() => { return new UserEntity() });
    const spySave = jest
      .spyOn(userRepository, "save")
      .mockImplementation((entity: UserEntity) => {
        const user = new UserEntity();
        user.id = 1;
        user.username = entity.username;
        user.password = entity.password;
        return Promise.resolve(user);
      });

    let userData = new UserPostRegisterDto();
    userData.username = "username";
    userData.password = "password";
    const userService = new UserService(userRepository,tokenRepository, new Config(process.env));
    const service = new ProfileService(userService);
    const registredUser = await service.register(userData);
    expect(registredUser).toBe(1);
  });

  it("get user data", async () => {
    const userRepository = new UserRepository();
    const tokenRepository = new TokenRepository();
    const user = new UserEntity();
    user.id = 1;
    user.username = "username";
    user.isActive = true;
    user.isAdmin = false;
    const spyFindOne = jest.spyOn(userRepository, 'findOne').mockImplementation((conditions?: FindConditions<UserEntity>) => {
      return Promise.resolve(user);
    });
    const userService = new UserService(userRepository,tokenRepository, new Config(process.env));
    const service = new ProfileService(userService);
    expect(await service.getOne(1)).toBe(user);
  });

  it("get user data (error)", async () => {
    
    const userRepository = new UserRepository();
    const tokenRepository = new TokenRepository();
    const user = new UserEntity();
    user.id = 1;
    user.username = "username";
    user.isActive = true;
    user.isAdmin = false;
    const spyFindOne = jest.spyOn(userRepository, 'findOne').mockImplementation((conditions?: FindConditions<UserEntity>) => {
      return Promise.resolve([user].filter(v => v.id === conditions)[0]);
    });
    const userService = new UserService(userRepository,tokenRepository, new Config(process.env));
    const service = new ProfileService(userService);
    try {
      await service.getOne(2);
      expect(false).toBe(true);
    } catch (e) {
      expect(true).toBe(true);
    }
  });

  it("get users list", async () => {
    
    const userRepository = new UserRepository();
    const tokenRepository = new TokenRepository();
    const user = new UserEntity();
    user.id = 1;
    user.username = "username";
    user.isActive = true;
    user.isAdmin = false;
    const spyFind = jest.spyOn(userRepository, 'find').mockImplementation((conditions?: FindConditions<UserEntity>) => {
      return Promise.resolve([user]);
    });
    const userService = new UserService(userRepository,tokenRepository, new Config(process.env));
    const service = new ProfileService(userService);
    expect((await service.getList()).length).toBe(1)
  });


  it("block user", async () => {
    
    const userRepository = new UserRepository();
    const tokenRepository = new TokenRepository();
    const user = new UserEntity();
    user.id = 1;
    user.username = "username";
    user.isActive = true;
    user.isAdmin = false;
    const spyFindOne = jest.spyOn(userRepository, 'findOne').mockImplementation((conditions?: FindConditions<UserEntity>) => {
      return Promise.resolve([user].filter(v => v.id === conditions)[0]);
    });
    const spySave = jest.spyOn(userRepository, 'save').mockImplementation((entity: UserEntity, options?: SaveOptions) => {
      return Promise.resolve(entity);
    });
    const userService = new UserService(userRepository,tokenRepository, new Config(process.env));
    const service = new ProfileService(userService);
    expect(await service.triggerBlockUser(1)).toBe(false);
  });

  it("delete user", async () => {
    
    const userRepository = new UserRepository();
    const tokenRepository = new TokenRepository();
    const spyFindOne = jest.spyOn(userRepository, 'delete').mockImplementation((conditions?: FindConditions<UserEntity>) => {
      const result = new DeleteResult();
      result.raw = 1;
      result.affected = 1;
      return Promise.resolve(result);
    });
    const userService = new UserService(userRepository,tokenRepository, new Config(process.env));
    const service = new ProfileService(userService);
    expect((await service.deleteUser(1)).affected).toBe(1);
  });
  it("delete user (not exist)", async () => {
    
    const userRepository = new UserRepository();
    const tokenRepository = new TokenRepository();
    const spyDelete = jest.spyOn(userRepository, 'delete').mockImplementation((conditions?: FindConditions<UserEntity>) => {
      const result = new DeleteResult();
      result.raw = 0;
      result.affected = 0;
      return Promise.resolve(result);
    });
    const userService = new UserService(userRepository,tokenRepository, new Config(process.env));
    const service = new ProfileService(userService);
    expect((await service.deleteUser(1)).affected).toBe(0);
  });

  it("update user", async () => {
    
    const userRepository = new UserRepository();
    const tokenRepository = new TokenRepository();
    const userUpdateDto = new UserPostUpdateDto();
    userUpdateDto.username = "username2";
    const user = new UserEntity();
    user.id = 1;
    user.username = "username";
    user.isActive = true;
    user.isAdmin = false;
    const spyFindOne = jest.spyOn(userRepository, 'findOne').mockImplementation((conditions?: FindConditions<UserEntity>) => {
      return Promise.resolve([user].filter(v => v.id === conditions)[0]);
    });
    const spySave = jest.spyOn(userRepository, 'save').mockImplementation((entity: UserEntity, options?: SaveOptions) => {
      return Promise.resolve(entity);
    });
    const userService = new UserService(userRepository,tokenRepository, new Config(process.env));
    const service = new ProfileService(userService);
    expect((await service.updateUser(1,userUpdateDto)).username).toBe("username2");
  });

  it("set admin right", async () => {
    
    const userRepository = new UserRepository();
    const tokenRepository = new TokenRepository();
    const user = new UserEntity();
    user.id = 1;
    user.username = "username";
    user.isActive = true;
    user.isAdmin = false;
    const spyFindOne = jest.spyOn(userRepository, 'findOne').mockImplementation((conditions?: FindConditions<UserEntity>) => {
      return Promise.resolve([user].filter(v => v.id === conditions)[0]);
    });
    const spySave = jest.spyOn(userRepository, 'save').mockImplementation((entity: UserEntity, options?: SaveOptions) => {
      return Promise.resolve(entity);
    });
    const userService = new UserService(userRepository,tokenRepository, new Config(process.env));
    const service = new ProfileService(userService);
    expect((await service.setUserAdminRights(1,true)).isAdmin).toBe(true);
  });
})