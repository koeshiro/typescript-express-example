
import "reflect-metadata";
import { ProfileController } from "../ProfileController.controller"
import { ProfileService } from "../ProfileService"
import { Config } from "@Config";
import { UserEntity, UserRepository, UserPostRegisterDto, UserPostUpdateDto, UserService, TokenRepository } from "@User";
import { Request as Req, Response as Res, } from "express";
import { DefaultResponseDto } from "../../Dtos/DefaultResponseDto";
jest.mock("@User/repositories/User.repository");
jest.mock("@User/repositories/Token.repository");
jest.unmock("@User/UserService")
require('dotenv').config();

describe("profile controller", () => {
  it("profile controller registerUser", async () => {
    const userRepository = new UserRepository();
    const tokenRepository = new TokenRepository();
    const spyCreate = jest.spyOn(userRepository, 'create').mockImplementation(() => { return new UserEntity() });
    const spySave = jest
      .spyOn(userRepository, "save")
      .mockImplementation((entity: UserEntity) => {
        const user = new UserEntity();
        user.id = 1;
        user.username = entity.username;
        user.password = entity.password;
        return Promise.resolve(user);
      });

    let userData = new UserPostRegisterDto();
    userData.username = "username";
    userData.password = "password";
    const userService = new UserService(userRepository, tokenRepository, new Config(process.env));
    const service = new ProfileService(userService);
    const controller = new ProfileController(service, new Config(process.env));
    const request = { body: { username: "username", password: "password" } } as Req;
    const response = { end() { }, json() { } } as Res;
    let res: DefaultResponseDto<number>;
    const spyReject = jest.spyOn(response, 'end').mockImplementation((r) => { });
    const spyResult = jest.spyOn(response, 'json').mockImplementation((body?: any) => { res = body; return body });
    await controller.registerUser(request, response);
    expect(res.data).toBe(1);
    expect(spySave).toBeCalled();
    expect(spyReject).not.toBeCalled();
  })
})