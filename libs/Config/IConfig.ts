export interface IConfig {
  get(keyL: string): any | null;
}
