import { injectable } from "tsyringe";
import { IConfig } from "./IConfig";

@injectable()
export class Config implements IConfig {
  protected data = new Map<string, string>();
  constructor(data:any) {
    const keys = Object.keys(data);
    for (const key of keys) {
      this.data.set(key, String(data[key]));
    }
  }
  get(key): string {
    if (this.data.has(key)) {
      return this.data.get(key);
    }
    return null;
  }
}
