import { autoInjectable } from "tsyringe";
import { Repository } from "typeorm";
import { UserEntity } from "../entity/User.entity";

@autoInjectable()
export class UserRepository extends Repository<UserEntity> { }
