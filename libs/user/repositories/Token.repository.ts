import { autoInjectable } from "tsyringe";
import { Repository } from "typeorm";
import { TokenEntity } from "../entity/Token.entity";

@autoInjectable()
export class TokenRepository extends Repository<TokenEntity> {}
