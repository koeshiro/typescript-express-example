import { createHmac } from "crypto";
import { autoInjectable, inject } from "tsyringe";
import { Config } from "@Config";
import { UserPostRegisterDto } from "./dtos/UserPostRegisterDto";
import { TokenRepository } from "./repositories/Token.repository";
import { UserRepository } from "./repositories/User.repository";
import { UserPostUpdateDto } from "./dtos/UserPostUpdateDto";
import jwt, { Algorithm } from "jsonwebtoken";
import { UserEntity } from "@User";
import { JwtDto } from "./dtos";

@autoInjectable()
export class UserService {
  constructor(
    protected userRepository: UserRepository,
    protected tokenRepository: TokenRepository,
    protected config: Config
  ) { }

  async getOne(userId: number) {
    const user = await this.userRepository.findOne(userId);
    if (user !== null && user !== undefined) {
      return user;
    } else {
      throw new Error("User not found");
    }
  }

  getList() {
    return this.userRepository.find();
  }

  async registerUser(userData: UserPostRegisterDto) {
    const user = this.userRepository.create();
    const keys = Object.keys(userData);
    for (const key of keys) {
      if (key !== "password" && userData[key].length > 0) {
        user[key] = userData[key];
      }
    }
    const secret = this.config.get("PASSWORD_SECRET_KEY");
    const hashWriter = createHmac("sha256", secret);
    user.password = hashWriter.update(userData.password).digest("hex");
    const result = await this.userRepository.save(user);
    return result.id;
  }
  async updateUser(userId: number, userData: UserPostUpdateDto) {
    const user = await this.userRepository.findOne(userId);
    const keys = Object.keys(userData);
    for (const key of keys) {
      if (key !== "password") {
        user[key] = userData[key];
      }
    }
    if ("password" in userData) {
      const secret = this.config.get("PASSWORD_SECRET_KEY");
      const hashWriter = createHmac("sha256", secret);
      user.password = hashWriter.update(userData.password).digest("hex");
    }
    return this.userRepository.save(user);
  }
  async deleteUser(userId: number) {
    return this.userRepository.delete(userId);
  }
  async triggerBlockUser(userId: number) {
    const user = await this.userRepository.findOne(userId);
    if (user === null) {
      throw new Error("User not found");
    }
    user.isActive = !user.isActive;
    await this.userRepository.save(user);
    return user.isActive;
  }

  async setUserAdminRights(userId: number, isAdmin: boolean) {
    const user = await this.userRepository.findOne(userId);
    user.isAdmin = isAdmin;
    return this.userRepository.save(user);
  }

  async deleteLoginToken(hash: string) {
    const [toketEntity] = await this.tokenRepository.find({ hash });
    if (toketEntity === null || toketEntity === undefined) {
      throw new Error("Token not found");
    }
    return this.tokenRepository.delete(toketEntity.id);
  }
  
  async testLoginToken(hash: string){
    const [toketEntity] = await this.tokenRepository.find({ hash });
    if (toketEntity === null || toketEntity === undefined) {
      return false 
    }
    return true;
  }

  async loginUser(username: string, password: string) {
    const hashWriter = createHmac(
      "sha256",
      this.config.get("PASSWORD_SECRET_KEY")
    );
    const [user] = await this.userRepository.find({
      username,
      password: hashWriter.update(password).digest("hex"),
      isActive: true,
    });
    if (user === null || user === undefined) {
      throw new Error("User not found");
    }
    return user;
  }
  async getUserToken(user: UserEntity) {
    const hashWriter = createHmac(
      "sha256",
      this.config.get("JWT_HASH_SECRET_KEY")
    );
    const data: JwtDto = { id: user.id, hash: hashWriter.update(`${user.id}-${Number(new Date())}-${this.config.get("JWT_HASH_SECRET_KEY")}-${Math.random()}`).digest("hex"), };
    const algorithms:Algorithm[] = this.config.get('JWT_ALGORITHMS').split(',') as Algorithm[];
    const algorithmIndex = Math.floor(Math.random() * algorithms.length - 1);
    const algorithm = algorithms[algorithmIndex>=0 && algorithmIndex < algorithms.length ? algorithmIndex : 0];
    const token = jwt.sign(data, this.config.get("JWT_SECRET_KEY"), {
      algorithm,
    });
    const tokenEntity = this.tokenRepository.create();
    tokenEntity.isActive = true;
    tokenEntity.hash = data.hash;
    tokenEntity.user = user;
    this.tokenRepository.save(tokenEntity);
    return token;
  }
  async isLogined(hash: string) {
    const [toketEntity] = await this.tokenRepository.find({ hash });
    if (toketEntity === null || toketEntity === undefined) {
      throw new Error("Token not found");
    }
    return !(toketEntity === null || toketEntity === undefined);
  }
  async isActive(userId: number) {
    const user = await this.userRepository.findOne(userId);
    if (user === null || user === undefined) {
      throw new Error("User not found");
    }
    return user.isActive;
  }
  async isAdmin(userId: number) {
    const user = await this.userRepository.findOne(userId);
    if (user === null || user === undefined) {
      throw new Error("User not found");
    }
    return user.isAdmin;
  }
}
