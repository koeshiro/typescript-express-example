export interface IUser {
  id: number;
  username: string;
  password: string;
  isActive: boolean;
  isAdmin: boolean;
  isService:boolean;
}