import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { IUser } from "../IUser";


@Entity()
export class UserEntity implements IUser {
  @PrimaryGeneratedColumn("increment")
  id: number;
  @Column({ nullable: false, unique: true })
  username: string;
  @Column({
    transformer: {
      from(v: string) {
        return v;
      },
      to(v: string) {
        return Buffer.from(v).toString("hex");
      },
    },
    select: false,
  })
  password: string;
  @Column()
  isActive: boolean;
  @Column()
  isAdmin: boolean;
  @Column()
  isService: boolean;
}
