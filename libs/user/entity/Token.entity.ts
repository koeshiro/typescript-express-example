import { Column, Entity, PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import { UserEntity } from "./User.entity";


@Entity()
export class TokenEntity {
  @PrimaryGeneratedColumn("increment")
  id: number;
  @Column({ nullable: false, unique: true })
  @ManyToOne(() => UserEntity, (user) => user.id, { eager: true })
  user: UserEntity;
  @Column()
  hash: string;
  @Column()
  isActive: boolean;
}
