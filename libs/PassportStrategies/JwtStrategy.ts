import { UserService, JwtDto } from "@User";
import { Strategy, StrategyOptions, VerifiedCallback } from "passport-jwt";
import { autoInjectable } from "tsyringe";

@autoInjectable()
export class JwtStrategy {
  constructor(protected service: UserService) { }
  getStrategy(options: StrategyOptions) {
    return new Strategy(options, (payload: JwtDto, done: VerifiedCallback) => {
      this.service.getOne(payload.id).then((u) => {
        done(null, u);
      }).catch((err) => {
        done(null, false);
      })
    });
  }
}