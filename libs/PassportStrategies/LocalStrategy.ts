import { UserService } from "@User";
import { Strategy, IStrategyOptions } from "passport-local";
import { autoInjectable } from "tsyringe";

@autoInjectable()
export class LocalStrategy {
  constructor(protected userService: UserService) { }
  getStrategy(options: IStrategyOptions) {
    return new Strategy(options, (username, password, done) => {
      this.userService.loginUser(username, password).then(
        (user) => {
          done(null, user);
        },
        (err) => {
          done(err, false);
        }
      );
    });
  }
}
