FROM node:latest
RUN apt install libpq-dev

# копируем исходный код
WORKDIR /usr/src/app

COPY package*.json ./
COPY tsconfig.json ./
RUN npm install

COPY . .
RUN npm run build
CMD ["npm", "run", "start"]
