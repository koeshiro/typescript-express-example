import "reflect-metadata";
import { container } from "tsyringe";
import { Request as Req, Response as Res, NextFunction } from "express";
import { Config } from "@Config";
import { ProfileController } from "./src/users/profile/ProfileController.controller";
import { createConnection } from "typeorm";
import { IUser } from "@User";
import { JwtStrategy } from "libs/PassportStrategies/JwtStrategy";
import { LocalStrategy } from "libs/PassportStrategies/LocalStrategy";
import { DefaultResponseDto } from "src/users/Dtos/DefaultResponseDto";
const express = require("express");

const bodyParser = require("body-parser");

require("dotenv").config();
createConnection()
  .then(() => {
    const app = express();
    app.use(bodyParser.urlencoded({ extended: false }));

    app.use(bodyParser.json());

    container.registerInstance("Config", new Config(process.env));

    const config = container.resolve<Config>("Config");
    const profileController = container.resolve(ProfileController);
    const jwt = container.resolve(JwtStrategy).getStrategy({
      jwtFromRequest(req) {
        const auth = req.header("Authorization");
        if (auth.length > 0 && auth.indexOf(" ") > 0) {
          return auth[1];
        } return null;
      },
      secretOrKey: config.get('JWT_SECRET_KEY'),
      algorithms: config.get('JWT_ALGORITHMS').split(',')
    });
    const local = container.resolve(LocalStrategy).getStrategy({});

    function forAdmin(req: Req & { user: IUser }, res: Res, next: NextFunction) {
      try {
        if (!('user' in req && req.user && 'id' in req.user && Number(req.user.id) > 0 && req.user.isActive && req.user.isAdmin)) {
          throw new Error("Access denied")
        }
        next()
      } catch (e) {
        const defaultResponseDto: DefaultResponseDto<null> = new DefaultResponseDto<null>();
        res.statusCode = 401;
        res.statusMessage = e.message;
        defaultResponseDto.error = true;
        defaultResponseDto.message = e.message;
        defaultResponseDto.data = null;
        res.json(defaultResponseDto)
      }
    }

    app.post("/users/profile", (req: Req, res: Res, next: NextFunction) => {
      profileController.registerUser(req, res);
    });

    app.get("/users/profile", (req: Req & { user: IUser }, res: Res, next: NextFunction) => {
      profileController.getUser(req, res);
    });
    app.put("/users/profile", jwt, (req: Req & { user: IUser }, res: Res, next: NextFunction) => {
      profileController.updateUser(req, res);
    });
    app.get("/users/profile/auth", jwt, (req: Req & { user: IUser }, res: Res, next: NextFunction) => {
      profileController.testUserJwt(req, res);
    });
    app.post("/users/profile/auth", local, (req: Req & { user: IUser }, res: Res, next: NextFunction) => {
      profileController.loginUserGetToken(req, res);
    });
    app.delete("/users/profile/auth", jwt, (req: Req & { user: IUser }, res: Res, next: NextFunction) => {
      profileController.deleteUserToken(req, res);
    });

    app.get("/users/list", jwt, forAdmin, (req: Req & { user: IUser }, res: Res, next: NextFunction) => {
      profileController.getList(req, res);
    });
    app.post("/users/list", jwt, forAdmin, (req: Req & { user: IUser, params: { id?: string } }, res: Res, next: NextFunction) => {
      profileController.registerUserByAdmin(req, res);
    });
    app.put("/users/list/:id", jwt, forAdmin, (req: Req & { user: IUser, params: { id?: string } }, res: Res, next: NextFunction) => {
      profileController.updateUserByAdmin(req, res);
    });
    app.put("/users/list/:id/trigger-block", jwt, forAdmin, (req: Req & { user: IUser, params: { id?: string } }, res: Res, next: NextFunction) => {
      profileController.triggerBlockUser(req, res);
    });
    app.put("/users/list/:id/set-admin-rights", jwt, forAdmin, (req: Req & { user: IUser, params: { id?: string } }, res: Res, next: NextFunction) => {
      profileController.setUserAdminRights(req, res);
    });
    app.delete("/users/list/:id", jwt, forAdmin, (req: Req & { user: IUser, params: { id?: string } }, res: Res, next: NextFunction) => {
      profileController.deleteUser(req, res);
    });

    app.get("/books", jwt)

    app.post("/books/booking", jwt)
    app.delete("/books/booking", jwt)

    app.get("/books/not-booked", jwt)
    app.get("/books/most-popular", jwt)
    app.get("/books/booked-by-book/:isbn", jwt, forAdmin)

    app.listen(Number(config.get("PORT")), config.get("HOST"), () =>
      console.log(`http://${config.get("HOST")}:${Number(config.get("PORT"))}`)
    );
  })
  .catch(console.error);
